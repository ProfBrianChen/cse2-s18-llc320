///CSE 2 Arithmetic
//Larissa Chow
public class Arithmetic
{
    public static void main(String[] args)
    {
        //Input Data
        int numPants = 3; //Number of pairs of pants
        double pantsPrice = 34.98;  //Cost per pair of pants
        
        int numShirts = 2;  //Number of sweatshirts
        double shirtPrice = 24.99;  //Cost per sweatshirt
        
        int numBelts = 1; //Number of belts
        double beltPrice = 33.99;  //Cost per belt
      
        double paSalesTax = 0.06; //The tax rate
      
        //Output Variables
        double totalCostOfPants = numPants * pantsPrice;  //total cost of pants
        double totalCostOfShirts = numShirts * shirtPrice;  //total cost of sweatshirts
        double totalCostOfBelts = numBelts * beltPrice;  //total cost of belts
        
        double pantsSalesTax = ((int)((totalCostOfPants * paSalesTax)*100))/100.0;  //sales tax charged on pants
        double shirtsSalesTax = ((int)((totalCostOfShirts * paSalesTax)*100))/100.0; //sales tax charges on sweatshirts
        double beltsSalesTax = ((int)((totalCostOfBelts * paSalesTax)*100))/100.0; //sales tax charged on belts
        
        double totalCostNoTax = ((int)((totalCostOfBelts + totalCostOfShirts + totalCostOfPants)*100))/100.0;  //total cost of purchases before tax
        double totalSalesTax = ((int)((totalCostNoTax * paSalesTax)*100))/100.0; //total sales tax
        double totalCost = ((int)((totalCostNoTax + totalSalesTax)*100))/100.0;  //total paid for transaction, including sales tax
      
        //Prints out calculations to terminal window
        System.out.println("The total cost of pants is $" + totalCostOfPants + ".");
        System.out.println("The total cost of sweatshirts is $" + (totalCostOfShirts) + ".");
        System.out.println("The total cost of belts is $" + (totalCostOfBelts) + ".");
        
        System.out.println();
      
        System.out.println("The sales tax charged on pants is $" + (pantsSalesTax) + ".");
        System.out.println("The sales tax charged on sweatshirts is $" + (shirtsSalesTax) + ".");
        System.out.println("The sales tax charged on belts is $" + (beltsSalesTax) + ".");
      
        System.out.println();
      
        System.out.println("The total cost of purchases before tax is $" + (totalCostNoTax) + ".");
        System.out.println("The total sales tax on this purchase is $" + (totalSalesTax) + ".");
        System.out.println("The total cost of this transaction, including sales tax, is $" + (totalCost) + ".");
      
    }
}