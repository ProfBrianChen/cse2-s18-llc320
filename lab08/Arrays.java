//CSE 2 Arrays
//
//Larissa Chow
//April 13, 2018

import java.util.Scanner;
import java.util.Random;

public class Arrays
{
  public static void main(String[] args)
  {
    Scanner scan = new Scanner(System.in);
    //Random rnd = new Random();
    String[] students = new String[5];
    System.out.println("Enter 5 student names:");
    for(int i = 0; i<5; i++)
    {
      students[i] = scan.nextLine();
    }
    int[] midterm = new int[5];
    Random rnd = new Random();
    System.out.println();
    System.out.println("Here are the midterm grades of the 5 students above:");
    for(int i = 0; i<5; i++)
    {
      midterm[i] = rnd.nextInt(101);
      System.out.println(students[i] + ": " + midterm[i]);
    }
  }
}