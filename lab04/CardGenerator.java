//CSE 2 Card Generator
//Pick a card, which is represented by a random number
//Larissa Chow
//February 16, 2018

public class CardGenerator
{
    //main method required for every Java program
    public static void main(String[] args)
    { 
        //random number generator
        int card = (int)(Math.random() * 52)+1;
        
        //string variables to be assigned values
        String suit = "";
        String id = "";
        
        //assigning the suit name
        if (card <= 13)  
          suit = "Diamonds";
        else if (card <= 26)
          suit = "Clubs";
        else if (card <= 39)
          suit = "Hearts";
        else if (card <= 52)
          suit = "Spades";
      
        //assigning the card identity
        switch (card)
        {
          case 1: case 14: case 27: case 40:
          id = "Ace";
          break;
          case 2: case 15: case 28: case 41:
          id = "2";
          break;
          case 3: case 16: case 29: case 42:
          id = "3";
          break;
          case 4: case 17: case 30: case 43:
          id = "4";
          break;
          case 5: case 18: case 31: case 44:
          id = "5";
          break;
          case 6: case 19: case 32: case 45:
          id = "6";
          break;
          case 7: case 20: case 33: case 46:
          id = "7";
          break;
          case 8: case 21: case 34: case 47:
          id = "8";
          break;
          case 9: case 22: case 35: case 48:
          id = "9";
          break;
          case 10: case 23: case 36: case 49:
          id = "10";
          break;
          case 11: case 24: case 37: case 50:
          id = "Jack";
          break;
          case 12: case 25: case 38: case 51:
          id = "Queen";
          break;
          case 13: case 26: case 39: case 52:
          id = "King";
          break;
        }
        
        //prints out name of randomly selected card
        System.out.println("You picked the " + id  + " of " + suit);
    }
}