//CSE 2 Yahtzee
//Perform a random roll of the dice and score the roll
//Larissa Chow
//February 20, 2018

import java.util.Scanner;

public class Yahtzee
{
    public static void main(String[] args)
    {
        Scanner myScanner = new Scanner(System.in);
        
        //setting the value of the dice
        int dice1 = 0;
        int dice2 = 0;
        int dice3 = 0;
        int dice4 = 0;
        int dice5 = 0;
        
        //giving the user the option between choice 1 and choice 2
        System.out.print("Enter 1 for random dice roll and 2 to type in 5 digit number: ");
        int choice1 = myScanner.nextInt();
        //if the user picks choice 1
        if (choice1 == 1)
        {
          dice1 = (int)(Math.random() * 6) + 1;
          dice2 = (int)(Math.random() * 6) + 1;
          dice3 = (int)(Math.random() * 6) + 1;
          dice4 = (int)(Math.random() * 6) + 1;
          dice5 = (int)(Math.random() * 6) + 1;
        } 
        
        //if the user picks choice 2
        else if (choice1 == 2)
        {
          System.out.println("Type out a 5 digit number representing the result of a specific roll. ");
          System.out.print("Enter the first number: ");
          dice1 = myScanner.nextInt();
          System.out.print("Enter the second number: ");
          dice2 = myScanner.nextInt();
          System.out.print("Enter the third number: ");
          dice3 = myScanner.nextInt();
          System.out.print("Enter the fourth number: ");
          dice4 = myScanner.nextInt();
          System.out.print("Enter the fifth number: ");
          dice5 = myScanner.nextInt();
        } 
      
          //checks to make sure that the numbers input from choice 2 are within the range 1-6 (numbers on a dice)
          boolean toggle = false;
          if((dice1 >= 1 && dice1 <= 6) && (dice2 >= 1 && dice2 <= 6) && (dice3 >=1 && dice3 <= 6) && 
             (dice4 >= 1 && dice4 <= 6) && (dice5 >= 1 && dice5 <= 6))
          {
            //if the boolean is true, the game will print out the input numbers
            toggle = true;
            System.out.println(dice1 + ", " + dice2 + ", " + dice3 + ", " + dice4 + ", " + dice5);
          }
          else
          {
            //if the boolean is false, the game will not let the user continue playing the game
            System.out.println("Error!");
          }
        //game will continue to play if boolean is true
        if (toggle)
        {
         //UPPER SECTION
         //setting the values of the 1s, 2s, 3s, 4s, 5s, 6s
         int aces = 0;
         int twos = 0;
         int threes = 0;
         int fours = 0;
         int fives = 0;
         int sixes = 0;
        
        //counts how many 1s, 2s, 3s, 4s, 5s, and 6s for each of the five dice
        switch (dice1)
         {
          case 1:
             aces = aces + 1;
          break;
          case 2:
             twos = twos + 1;
          break;
          case 3:
             threes = threes + 1;
          break;
          case 4:
             fours = fours + 1;
          break;
          case 5:
             fives = fives + 1;
          break;
          case 6:
             sixes = sixes + 1;
        }  
        switch (dice2)
        {
          case 1:
             aces = aces + 1;
          break;
          case 2:
             twos = twos + 1;
          break;
          case 3:
             threes = threes + 1;
          break;
          case 4:
             fours = fours + 1;
          break;
          case 5:
             fives = fives + 1;
          break;
          case 6:
             sixes = sixes + 1;
        }
        switch (dice3)
        {
          case 1:
             aces = aces + 1;
          break;
          case 2:
             twos = twos + 1;
          break;
          case 3:
             threes = threes + 1;
          break;
          case 4:
             fours = fours + 1;
          break;
          case 5:
             fives = fives + 1;
          break;
          case 6:
             sixes = sixes + 1;
        }
        switch (dice4)
        {
          case 1:
             aces = aces + 1;
          break;
          case 2:
             twos = twos + 1;
          break;
          case 3:
             threes = threes + 1;
          break;
          case 4:
             fours = fours + 1;
          break;
          case 5:
             fives = fives + 1;
          break;
          case 6:
             sixes = sixes + 1;
        }
        switch (dice5)
        {
          case 1:
             aces = aces + 1;
          break;
          case 2:
             twos = twos + 1;
          break;
          case 3:
             threes = threes + 1;
          break;
          case 4:
             fours = fours + 1;
          break;
          case 5:
             fives = fives + 1;
          break;
          case 6:
             sixes = sixes + 1;
        }
          
        //calculating and declaring the value of the upper section initial total and the upper section total including bonus
        int uppersection = (aces * 1) + (twos * 2) + (threes * 3) + (fours * 4) + (fives * 5) + (sixes * 6);
        System.out.println("Upper section initial total: " + uppersection);
        //if the upper section initial total is above 63, 35 points will be added to the upper section total
        if (uppersection > 63)
          uppersection = uppersection + 35;
        System.out.println("Upper section total including bonus: " + uppersection);
          
        //LOWER SECTION
        //setting three of a kind
        int threeofkind = 0;
        if (aces == 3)
          threeofkind = (aces * 1);
        else if (twos == 3)
          threeofkind = (twos * 2);
        else if (threes == 3)
          threeofkind = (threes * 3);
        else if (fours == 3)
          threeofkind = (fours * 4);
        else if (fives == 3)
          threeofkind = (fives * 5);
        else if (sixes == 3)
          threeofkind = (sixes * 6);
         
        //setting four of a kind
        int fourofkind = 0;
        if (aces == 4)
          fourofkind = (aces * 1);
        else if (twos == 4)
          fourofkind = (twos * 2);
        else if (threes == 4)
          fourofkind = (threes * 3);
        else if (fours == 4)
          fourofkind = (fours * 4);
        else if (fives == 4)
          fourofkind = (fives * 5);
        else if (sixes == 4)
          fourofkind = (sixes * 6);
          
        //setting Yahtzee
        int yahtzee = 0;
        if (aces == 5 || twos == 5 || threes == 5 || fours == 5 || fives == 5 || sixes == 5)
          yahtzee = 50;
          
        //setting full house
        int fullhouse = 0;
        //if there are 2 aces
        if (aces == 2)
        {
          if (twos == 3 || threes == 3 || fours == 3 || fives == 3 || sixes == 3)
          fullhouse = 25;
        }
        //if there are 2 twos
        else if (twos == 2)
        {
          if (aces == 3 || threes == 3 || fours == 3 || fives == 3 || sixes == 3)
          fullhouse = 25;
        }
        //if there are 2 threes
        else if (threes == 2)
        {
          if (aces == 3 || twos == 3 || fours == 3 || fives == 3 || sixes == 3)
          fullhouse = 25;
        }
        //if there are 2 fours
        else if (fours == 2)
        {
          if (aces == 3 || twos == 3 || threes == 3 || fives == 3 || sixes == 3)
          fullhouse = 25;
        }
        //if there are 2 fives
        else if (fives == 2)
        {
          if (aces == 3 || twos == 3 || threes == 3 || fours == 3 || sixes == 3)
          fullhouse = 25;
        }
        //if there are 2 sixes
        else if (sixes == 2)
        {
          if (aces == 3 || twos == 3 || threes == 3 || fours == 3 || fives == 3)
          fullhouse = 25;
        }
         
        //setting small straight component
        int smallstraight = 0;
        if (aces == 2 && twos == 1 && threes == 1 && fours == 1)  //any variation of 1, 1, 2, 3, 4
          smallstraight = 30;
        else if (aces == 1 && twos == 2 && threes == 1 && fours == 1) //any variation of 1, 2, 2, 3, 4
          smallstraight = 30;
        else if (aces == 1 && twos == 1 && threes == 2 && fours == 1) //any variation of 1, 2, 3, 3, 4
          smallstraight = 30;
        else if (aces == 1 && twos == 1 && threes == 1 && fours == 2) //any variation of 1, 2, 3, 4, 4
          smallstraight = 30;
        else if (aces == 1 && twos == 1 && threes == 1 && fours == 1 && sixes == 1) //any variation of 1, 2, 3, 4, 6
          smallstraight = 30;
        
        else if (twos == 2 && threes == 1 && fours == 1 && fives == 1)  //any variation of 2, 2, 3, 4, 5
          smallstraight = 30;
        else if (twos == 1 && threes == 2 && fours == 1 && fives == 1)  //any variation of 2, 3, 3, 4, 5
          smallstraight = 30;
        else if (twos == 1 && threes == 1 && fours == 2 && fives == 1)  //any variation of 2, 3, 4, 4, 5
          smallstraight = 30;
        else if (twos == 1 && threes == 1 && fours == 1 && fives == 2)  //any variation of 2, 3, 4, 5, 5
          smallstraight = 30;
        
        else if (threes == 2 && fours == 1 && fives == 1 && sixes == 1) //any variation of 3, 3, 4, 5, 6
          smallstraight = 30;
        else if (threes == 1 && fours == 2 && fives == 1 && sixes == 1) //any variation of 3, 4, 4, 5, 6
          smallstraight = 30;
        else if (threes == 1 && fours == 1 && fives == 2 && sixes == 1) //any variation of 3, 4, 5, 5, 6
          smallstraight = 30;
        else if (threes == 1 && fours == 1 && fives == 1 && sixes == 2) //any variation of 3, 4, 5, 6, 6
          smallstraight = 30;
        else if (aces == 1 && threes == 1 && fours == 1 && fives == 1 && sixes == 1)  //any variation of 1, 3, 4, 5, 6
          smallstraight = 30;
          
        //setting large straight component
        int largestraight = 0;
        if (aces == 1 && twos == 1 && threes == 1 && fours == 1 && fives == 1)  //any variation of 1, 2, 3, 4, 5
          largestraight = 40;
        else if (twos == 1 && threes == 1 && fours == 1 && fives == 1 && sixes == 1)  //any variation of 2, 3, 4, 5, 6
          largestraight = 40;
        
        //setting chance component
        int chance = 0;
        chance = uppersection;
        
        //calculating and declaring the value of the lower section total
        int lowersection = threeofkind + fourofkind + yahtzee + fullhouse + smallstraight + largestraight + chance;
        System.out.println("Lower section total: " + lowersection);
          
        int grandtotal = uppersection + lowersection;
        System.out.println("Grand total: " + grandtotal);
        } //end of toggle
    } //end of main method
} //end of class