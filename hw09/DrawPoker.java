//CSE 2 DrawPoker
//Larissa Chow
//April 10, 2018

import java.util.Random;

public class DrawPoker
{
  public static void main(String[] args)
  {
    int[] deck = new int[52]; //create deck of 52 cards
    String suit = "";
    String number = "";
    for(int i = 0; i < deck.length; i++)
    {
      deck[i] = i;
    } //end of for loop
    
    int[] shuffleDeck = shuffle(deck);  //call shuffle method
    
    int P1turn1 = dealP1(shuffleDeck);  //deal P1 a card
    int P2turn1 = dealP2(shuffleDeck);  //deal P2 a card
    System.out.println("P1: " + number(P1turn1) + " of " + suit(P1turn1));
    System.out.println("P2: " + number(P2turn1) + " of " + suit(P2turn1));
    
    int P1turn2 = dealP1(shuffleDeck);  //deal P1 a card
    int P2turn2 = dealP2(shuffleDeck);  //deal P2 a card
    System.out.println("P1: " + number(P1turn2) + " of " + suit(P1turn2));
    System.out.println("P2: " + number(P2turn2) + " of " + suit(P2turn2));
    
    int P1turn3 = dealP1(shuffleDeck);  //deal P1 a card
    int P2turn3 = dealP2(shuffleDeck);  //deal P2 a card
    System.out.println("P1: " + number(P1turn3) + " of " + suit(P1turn3));
    System.out.println("P2: " + number(P2turn3) + " of " + suit(P2turn3));
    
    int P1turn4 = dealP1(shuffleDeck);  //deal P1 a card
    int P2turn4 = dealP2(shuffleDeck);  //deal P2 a card
    System.out.println("P1: " + number(P1turn4) + " of " + suit(P1turn4));
    System.out.println("P2: " + number(P2turn4) + " of " + suit(P2turn4));
    
    int P1turn5 = dealP1(shuffleDeck);  //deal P1 a card
    int P2turn5 = dealP2(shuffleDeck);  //deal P2 a card
    System.out.println("P1: " + number(P1turn5) + " of " + suit(P1turn5));
    System.out.println("P2: " + number(P2turn5) + " of " + suit(P2turn5));
    
    String[] P1suit = {suit(P1turn1), suit(P1turn2), suit(P1turn3), suit(P1turn4), suit(P1turn5)};  
    //adds P1's card suits into an array
    String[] P2suit = {suit(P2turn1), suit(P2turn2), suit(P2turn3), suit(P2turn4), suit(P2turn5)};
    //adds P2's card suits into an array
    
    int[] P1number = {P1turn1, P1turn2, P1turn3, P1turn4, P1turn5}; //adds P1's card numbers into an array
    int[] P2number = {P2turn1, P2turn2, P2turn3, P2turn4, P2turn5}; //adds P2's card numbers into an array
    
    int P1 = 0; //sets P1's points to zero
    int P2 = 0; //sets P2's points to zero
    
    if(pair(P1number) == true)  //if P1 has a pair
    {
      System.out.println("P1 has a pair. So cool...");
      P1 += 1;  //add a point
    }
    if(pair(P2number) == true)  //if P2 has a pair
    {
      System.out.println("P2 has a pair. Whoopdeedoo...");
      P2 += 1;  //add a point
    }
    if(threeofkind(P1number) == true) //if P1 has three of a kind
    {
      System.out.println("P1 has three of a kind. Holy frijoles!");
      P1 += 1;  //add a point
    }
    if(threeofkind(P2number) == true) //if P2 has three of a kind
    {
      System.out.println("P2 has three of a kind! Cool beans!");
      P2 += 1;  //add a point
    }
    if(flush(P1suit) == true) //if P1 has a flush
    {
      System.out.println("P1 has a flush! Amazing!");
      P1 += 1;  //add a point
    }
    if(flush(P2suit) == true) //if P2 has a flush
    {
      System.out.println("P2 has a flush! Crazy!");
      P2 += 1;  //add a point
    }
    if(fullhouse(P1number) == true) //if P1 has a full house
    {
      System.out.println("P1 has a full house! Wow!");
      P1 += 1;  //add a point
    }
    if(fullhouse(P2number) == true) //if P2 has a full house
    {
      System.out.println("P2 has a full house! Awesome sauce!");
      P2 += 1;  //add a point
    }
    System.out.println("The current score is:");
    System.out.println("P1: " + P1);
    System.out.println("P2: " + P2);
    
    if(P1 > P2) //if P1 has more points than P2
    {
      System.out.println("P1 is the winner! Congrats buddy");
    }
    if(P2 > P1) //P2 has more points than P1
    {
      System.out.println("P2 is the winner! You worked hard for this. I'm so proud of you.");
    }
    
    if(P1 == 0 && P2 == 0)  //if neither player scored any points
    {
      System.out.println("Looks like both players drew some pretty bad cards so the winner is the player with the highest card.");
      int highP1 = (P1number[0]%13);  //sets variable equal to first number in P1number array
      for(int i = 1; i < P1number.length; i++)
      {
        if((P1number[i]%13) > highP1) //if one of the other numbers in the array is larger than the first
        {
          highP1 = P1number[i]; //that highest number is equal to the variable
        }
      }
      
      int highP2 = (P2number[0]%13);
      for(int j = 1; j < P2number.length; j++)
      {
        if((P2number[j]%13) > highP2)
        {
          highP2 = P2number[j];
        }
      }

      if(highP1 > highP2) //if P1 highest card is higher than P2 highest card
      {
        System.out.println("P1 is the winner! Congrats buddy");
      }
      if(highP2 > highP1) //if P2 highest card is higher than P1 highest card
      {
        System.out.println("P2 is the winner! You worked hard for this. I'm so proud of you.");
      }
    }
  } //end of main method
  
  public static int[] shuffle(int[] deck) //shuffles the deck
  {
    int first, second;
    Random rnd = new Random();
    for(int i = deck.length - 1; i > 0; i--)  //scrambles deck
    {
      first = rnd.nextInt(i+1);
      second = deck[first];
      deck[first] = deck[i];
      deck[i] = second;
    }
    return deck;
  } //end of shuffle method
  
 public static int dealP1(int[] shuffleDeck)  //deals out random cards from deck to P1
  {
    Random rnd = new Random();
    int turn = 0;
    turn = shuffleDeck[rnd.nextInt(52)];
    return turn;
  } //end of dealP1 method
  
  public static int dealP2(int[] shuffleDeck) //deals out random cards from deck to P2
  {
    Random rnd = new Random();
    int turn = 0;
    turn = shuffleDeck[rnd.nextInt(52)];
    return turn;
  } //end of dealP2 method
    
  public static String suit(int turn) //decides which suit each card is depending on the number
  {
    String sandwich = "";
    if(turn/13 == 0)
      {
        sandwich = "diamonds";
      }
      else if(turn/13 == 1)
      {
        sandwich = "clubs";
      }
      else if(turn/13 == 2)
      {
        sandwich = "hearts";
      }
      else if(turn/13 == 3)
      {
        sandwich = "spades";
      }
    return sandwich;
  } //end of suit method
  
  public static String number(int jelly)  //sets numbers to appropriate card values
  {
    String peanut = "";
    if(jelly%13 == 0)
      peanut = "Ace";
    else if(jelly%13 == 1)
      peanut = "Two";
    else if(jelly%13 == 2)
      peanut = "Three";
    else if(jelly%13 == 3)
      peanut = "Four";
    else if(jelly%13 == 4)
      peanut = "Five";
    else if(jelly%13 == 5)
      peanut = "Six";
    else if(jelly%13 == 6)
      peanut = "Seven";
    else if(jelly%13 == 7)
      peanut = "Eight";
    else if(jelly%13 == 8)
      peanut = "Nine";
    else if(jelly%13 == 9)
      peanut = "Ten";
    else if(jelly%13 == 10)
      peanut = "Jack";
    else if(jelly%13 == 11)
      peanut = "Queen";
    else if(jelly%13 == 12)
      peanut = "King";
    return peanut;
  } //end of number method
  
  public static boolean pair(int[] Pnumber) //checks to see if any player has two of the same card numbers
  {
    for(int i = 1; i < Pnumber.length; i++)
    {
      if(Pnumber[i]%13 == Pnumber[0]%13)  //if first card in hand is equal to any other card in hand
      {
        return true;
      }
    }
    return false;
  } //end of pair method
  
  public static boolean threeofkind(int[] Pnumber)  //checks to see if any players has three of a kind
  {
    for(int i = 1; i < Pnumber.length; i++)
    {
      for(int j = i+1; j < Pnumber.length; j++)
      {
        if(Pnumber[i]%13 == Pnumber[0]%13 && Pnumber[j]%13 == Pnumber[0]%13)  
        //if any two numbers from hand are equal to first card in hand
        {
          return true;
        }
      }
    }
    return false;
  } //end of threeofkind method
  
  public static boolean flush(String[] Psuit) //checks to see if any player has a flush
  {
    int tortilla = 0;
    for(int i = 1; i < Psuit.length; i++)
    {
      if(Psuit[i] == Psuit[0])  //if the suit of the card is equal to the suit of the first card
      {
        tortilla += 1;  //add one to variable
      }
    }
    if(tortilla == 4) //if all five cards have same suit
    {
      return true;
    }
    return false;
  } //end of flush method
  
  public static boolean fullhouse(int[] Pnumber)  //checks to see if any player has a full house
  {
    if(pair(Pnumber) && threeofkind(Pnumber)) //if has a pair and three of a kind
    {
      return true;
    }
    return false;
  } //end of fullhouse method
}