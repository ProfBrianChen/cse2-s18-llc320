//CSE 2 Methods
//
//Larissa Chow
//March 23, 2018

import java.util.Random;
import java.util.Scanner;

public class Methods
{
    public static void main(String[] args)
    { 
      String yesno = "";
      do
      {
        Scanner scan = new Scanner(System.in);
        String a1 = "no";
        String a2 = "no";
        String a3 = "no";
        String a4 = "no";
        //String yesno = "yes";
        System.out.println("The " + Adjective(a1) + " " + Adjective(a1) + " " + Subject(a2) + " " + 
                           PastVerb(a3) + " the " + Adjective(a1) + " " + ObjectNoun(a4) + ".");
        System.out.println("Another sentence? (yes or no) ");
        yesno = scan.next();
      }
      while(yesno.equals("yes"));
    }
  
    public static String Adjective(String a)
    {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);  //generates random integers less than 10
        switch(randomInt)
        {
          case 0:
            a = "pretty";
          break;
          case 1:
            a = "quick";
          break;
          case 2:
            a = "ugly";
          break;
          case 3:
            a = "sassy";
          break;
          case 4:
            a = "slow";
          break;
          case 5:
            a = "tired";
          break;
          case 6:
            a = "angry";
          break;
          case 7:
            a = "sad";
          break;
          case 8:
            a = "funny";
          break;
          case 9:
            a = "evil";
        }
        return a;
    }
  
    public static String Subject(String b) 
    {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);  //generates random integers less than 10
        switch(randomInt)
          {
            case 0:
              b = "fox";
            break;
            case 1:
              b = "bear";
            break;
            case 2:
              b = "doggo";
            break;
            case 3:
              b = "cat";
            break;
            case 4:
              b = "narwhal";
            break;
            case 5:
              b = "unicorn";
            break;
            case 6:
              b = "dragon";
            break;
            case 7:
              b = "wyvern";
            break;
            case 8:
              b = "mermaid";
            break;
            case 9:
              b = "ant";
          }
          return b;
    }
    
    public static String PastVerb(String c)
    {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);  //generates random integers less than 10
        switch(randomInt)
        {
          case 0:
            c = "passed";
          break;
          case 1:
            c = "pushed";
          break;
          case 2:
            c = "carried";
          break;
          case 3:
            c = "moved";
          break;
          case 4:
            c = "advised";
          break;
          case 5:
            c = "admired";
          break;
          case 6:
            c = "annoyed";
          break;
          case 7:
            c = "avoided";
          break;
          case 8:
            c = "choked";
          break;
          case 9:
            c = "destroyed";
        }
        return c;
    }
  
    public static String ObjectNoun(String d)
    {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);  //generates random integers less than 10
        switch(randomInt)
        {
          case 0:
            d = "chair";
          break;
          case 1:
            d = "ghost";
          break;
          case 2:
            d = "iguana";
          break;
          case 3:
            d = "whale";
          break;
          case 4:
            d = "t-rex";
          break;
          case 5:
            d = "wooly mammoth";
          break;
          case 6:
            d = "Big Foot";
          break;
          case 7:
            d = "wheelchair";
          break;
          case 8:
            d = "saxophone";
          break;
          case 9:
            d = "shoe";
        }
        return d;
    }
}