//CSE 2 Methods2
//
//Larissa Chow
//March 23, 2018

import java.util.Scanner;
import java.util.Random;

public class Methods2
{
    public static void main(String[] args)
    {
      Scanner scan = new Scanner(System.in);
      String answer = "";
      do
      {
        Thesis();
        System.out.println("Another sentence? (yes or no) ");
        answer = scan.next();
      }
      while(answer.equals("yes"));
    }
    
    public static void Thesis()
    { 
      String a1 = "no";
      String a2 = "no";
      String a3 = "no";
      String a4 = "no";
      String hello1 = Adjective(a1);
      String hello2 = Adjective(a1);
      String hello3 = Subject(a2);
      String hello4 = PastVerb(a3);
      String hello5 = Adjective(a1);
      String hello6 = ObjectNoun(a4);
      System.out.println("The " + hello1 + " " + hello2 + " " + hello3 + " " + hello4 + " the " + hello5 + " " + hello6 + ".");
      
      System.out.println("This " + hello3 + " was particularly " + Adjective(a1) + " to " + hello5 + " " + hello6 
                         + "s.");
      System.out.println("It used " + ObjectNoun(a4) + "s to spew " + Subject(a2) + "s at the " + hello5 + " " + hello6 + ".");
      System.out.println("The " + hello3 + " " + PastVerb(a3) + " her " + ObjectNoun(a4) + ".");
    }
  
    public static String Adjective(String a)
    {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);  //generates random integers less than 10
        switch(randomInt)
        {
          case 0:
            a = "pretty";
          break;
          case 1:
            a = "quick";
          break;
          case 2:
            a = "ugly";
          break;
          case 3:
            a = "sassy";
          break;
          case 4:
            a = "slow";
          break;
          case 5:
            a = "tired";
          break;
          case 6:
            a = "angry";
          break;
          case 7:
            a = "sad";
          break;
          case 8:
            a = "funny";
          break;
          case 9:
            a = "evil";
        }
        return a;
    }
  
    public static String Subject(String b) 
    {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);  //generates random integers less than 10
        switch(randomInt)
          {
            case 0:
              b = "fox";
            break;
            case 1:
              b = "bear";
            break;
            case 2:
              b = "doggo";
            break;
            case 3:
              b = "cat";
            break;
            case 4:
              b = "narwhal";
            break;
            case 5:
              b = "unicorn";
            break;
            case 6:
              b = "dragon";
            break;
            case 7:
              b = "wyvern";
            break;
            case 8:
              b = "mermaid";
            break;
            case 9:
              b = "ant";
          }
          return b;
    }
    
    public static String PastVerb(String c)
    {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);  //generates random integers less than 10
        switch(randomInt)
        {
          case 0:
            c = "passed";
          break;
          case 1:
            c = "pushed";
          break;
          case 2:
            c = "carried";
          break;
          case 3:
            c = "moved";
          break;
          case 4:
            c = "advised";
          break;
          case 5:
            c = "admired";
          break;
          case 6:
            c = "annoyed";
          break;
          case 7:
            c = "avoided";
          break;
          case 8:
            c = "choked";
          break;
          case 9:
            c = "destroyed";
        }
        return c;
    }
  
    public static String ObjectNoun(String d)
    {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);  //generates random integers less than 10
        switch(randomInt)
        {
          case 0:
            d = "chair";
          break;
          case 1:
            d = "ghost";
          break;
          case 2:
            d = "iguana";
          break;
          case 3:
            d = "whale";
          break;
          case 4:
            d = "t-rex";
          break;
          case 5:
            d = "wooly mammoth";
          break;
          case 6:
            d = "Big Foot";
          break;
          case 7:
            d = "wheelchair";
          break;
          case 8:
            d = "saxophone";
          break;
          case 9:
            d = "shoe";
        }
        return d;
    }
}