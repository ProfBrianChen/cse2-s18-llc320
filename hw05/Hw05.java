//CSE 2 Hw05
//Create loops that ask user for information regarding a class he/she is currently taking
//Larissa Chow
//March 6, 2018

import java.util.Scanner;

public class Hw05
{
    //main method required for every Java program
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        //asks for name of course
        System.out.println("Course name: ");
        while (!scan.hasNext()) //if the input is not equal to a string
        {
            System.out.println("Error! Please type in the name of the course:");  //prints out an error message
            scan.next();  //runs the loop again to check if the answer is a string
        }
        String coursename = scan.nextLine();  //prompts the user to input a response
      
        //asks for department number
        System.out.println("Department number: ");
        while (!scan.hasNextInt())  //if the input is not equal to an integer
        {
            System.out.println("Error! Please type in an integer.");  //prints out an error message
            System.out.println("Department number: ");  //asks user to input department number again
            scan.next();  //runs the loop again to check if the answer is an integer
        }
        int depnumber = scan.nextInt(); //stores the input variable 
        
        //asks for start time of class
        System.out.println("Class start time (XX:XX): ");
        String time = scan.next();  //prompts the user to input a response
        String timePattern = "\\d{2}:\\d{2}$";  //sets the time format to (XX:XX)
        while (!time.matches(timePattern))  //if the input response is not equal to the time format (XX:XX)
        {
          System.out.println("Error! Please type in a time in the following format (XX:XX): "); //prints out an error message
          System.out.println("Class start time (XX:XX): "); //asks the user to input class start time again
          time = scan.next(); //sets time variable equal to scan
        }
      
        //asks how many times a week the class meets
        System.out.println("How many times a week does this class meet? ");
        while (!scan.hasNextInt())  //if the input is not equal to an integer
        {
            System.out.println("Error! Please type in an integer.");  //prints out an error message
            System.out.println("How many times a week does this class meet? "); //asks user to input the number of times the class meets again
            scan.next();  //runs the loop again to check if the answer is an integer
        }
        int frequency = scan.nextInt(); //stores frequency variable
      
        //asks the name of the instructor
        System.out.println("Instructor Last Name: ");
        while (!scan.hasNext()) //if the input is not equal to a string
        {
            System.out.println("Error! Please type in the last name of the instructor: ");  //prints out an error message
            scan.next();  //runs the loop again to check if the answer is a string
        }
        String instructor = scan.next();  //prompts the user to input a response
      
        //asks the number of students in the class
        System.out.println("Number of students in the class: ");
        while (!scan.hasNextInt())  //if the input is not equal to an integer
        {
            System.out.println("Error! Please type in an integer.");  //prints out an error message
            System.out.println("Number of students in the class: ");  //asks user to input number of students in the class again
            scan.next();
        }
        int students = scan.nextInt();  //stores student variable
    } //end of main method
} //end of class