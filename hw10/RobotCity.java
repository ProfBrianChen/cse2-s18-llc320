//CSE 2 RobotCity
//Larissa Chow
//April 24, 2018

import java.util.Random;

public class RobotCity
{
  public static void main(String[] args)
  {
    Random rnd = new Random();
    
    int[][] city = buildCity(); //declares buildCity method
    System.out.println("Here we have a peaceful city full of peaceful citizens...");
    display(city);  //displays the peaceful city
    
    int k = rnd.nextInt(city.length * city[0].length);  //sets a random number of robots
    int[][] initialInvasion = invade(city, k);  //declares invade method
    
    System.out.println("OH NO! ROBOTS HAVE BEGUN TO ATTACK THE CITY!");
    display(city);  //displays the invaded city
    
    for(int i=0; i<5; i++)  //updates the invaded city 5 more times
    {
      update(city);
      System.out.println("Day " + (i+1));
      display(city);
    }
    
    System.out.println("We have suffered major casualties but it seems that the robots have begun to depart from the city.");
    System.out.println("An Angel came in and saved the day bois");
  } //end of main method
  
  public static int[][] buildCity() //builds the peaceful city
  {
    Random rnd = new Random();
    
    int[][] cityLifeFam = new int[rnd.nextInt(6)+10][rnd.nextInt(6)+10];  //sets random numbers from 10-15
    
    for(int i=0; i<cityLifeFam.length; i++)
    {
      for(int j=0; j<cityLifeFam[i].length; j++)
      {
        cityLifeFam[i][j] = rnd.nextInt(900)+100; //random numbers in city array anywhere from 100-999
      }
    }
    return cityLifeFam;
  } //end of buildCity method
  
  public static void display(int[][] city)  //displays the city
  {
    for(int i=0; i<city.length; i++)
    {
      System.out.print("||");
      for(int j=0; j<city[0].length; j++)
      {
        System.out.print(" ");
        System.out.printf("%4d", city[i][j]); //prints 4 spots with right side format
        System.out.print(" ");
      }
      System.out.println("||");
    }
    System.out.println();
  } //end of display method
  
  public static int[][] invade(int[][] city, int k) //robot invasion
  {
    Random rnd = new Random();

    for(int i=0; i<k; i++)
    {
      int a = rnd.nextInt(city.length);
      int b = rnd.nextInt(city[0].length);
      city[a][b] = city[a][b] * (-1); //randomly assigns number in city to negative value
    }
    return city;
  } //end of invade method
  
  public static void update(int[][] city) //updates the city after invasion
  {
    for(int i=0; i<city.length; i++)
    {
      boolean currentSign = true; //current sign
      boolean previousSign = true;  //previous sign
      for(int j=0; j<city[0].length; j++)
      {
        if(previousSign) //if previousSign is positive
        {
          if(city[i][j] > 0)  //if current int is positive
          {
            continue; //don't do anything
          }
          else if(city[i][j] < 0) //if current int is negative
          {
            currentSign = false;  //set currentSign to negative
            city[i][j] = city[i][j] * (-1); //set current int to positive
            previousSign = currentSign; //set previousSign to negative too
          }
        }
        else  //if previousSign is negative
        {
          if(city[i][j] < 0)  //if current int is negative
          {
            continue; //don't do anything
          }
          else if(city[i][j] > 0) //if current int is positive
          {
            currentSign = true; //set currentSign to positive
            city[i][j] = city[i][j] * (-1); //set current int to negative
            previousSign = currentSign; //set previousSign to positive too
          }
        }
      } //end of j for loop
    } //end of i for loop
  } //end of update method
} //end of class