///CSE 2 Welcome Class
//Larissa Chow
public class WelcomeClass   
{

    public static void main(String[] args) 
    {
        //Prints out design of my Lehigh username to the terminal window.
        System.out.println("  -----------");
        System.out.println("  | WELCOME |");
        System.out.println("  -----------");
        System.out.println("  ^  ^  ^  ^  ^  ^");
        System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
        System.out.println("<-L--L--C--3--2--0->");
        System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
        System.out.println("  v  v  v  v  v  v");
    }
}
       