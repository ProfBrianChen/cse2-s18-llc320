//CSE 2 Pyramid
//Larissa Chow
//February 13, 2018

import java.util.Scanner;

public class Pyramid
{
    public static void main(String[] args)
    {
        Scanner myScanner = new Scanner(System.in);
        
        //input
        System.out.print("The square side of the pyramid is (input length): ");
        double lengthPyramid = myScanner.nextDouble();
        
        System.out.print("The height of the pyramid is (input height): ");
        double heightPyramid = myScanner.nextDouble();
      
        //output
        double volumePyramid;
        volumePyramid = (lengthPyramid * lengthPyramid * heightPyramid)/3; //calculates volume of pyramid
        System.out.println("The volume inside the pyramid is: " + volumePyramid + ".");
    }
}
    
          
    