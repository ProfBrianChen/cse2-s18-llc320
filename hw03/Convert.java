//CSE 2 Convert
//Larissa Chow
//February 13, 2018

import java.util.Scanner;

public class Convert
{
    public static void main(String[] args)
    {
        Scanner myScanner = new Scanner(System.in);
          
        //input
        System.out.print("Enter the affected area in acres: ");
        double affectedAcres = myScanner.nextDouble();
        
        System.out.print("Enter the rainfall in the affected area in inches: ");
        double rainInches = myScanner.nextDouble();
      
        //output
        double quantityRain;
        double a = Math.pow(10, -5);  //exponential power to convert inches to miles
        double b = (affectedAcres * 0.0015625); //acres to square miles conversion
        double c = (rainInches * (1.5783 * a)); //inches to miles
        quantityRain = (b * c); //square miles times miles to calculate quantity of rain
        System.out.println(quantityRain + " cubic miles");
    }
}