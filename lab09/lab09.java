//CSE 2 lab09
//Pass arrays inside methods
//Larissa Chow
//April 13, 2018

import java.util.Arrays;

public class lab09
{
  public static void main(String[] args)
  {
    int array0[] = new int[] {2, 4, 6, 8, 10, 12, 14, 16};
    
    int array1[] = copy(array0);
    int array2[] = copy(array0);
    
    inverter(array0);
    inverter2(array1);
      System.out.println("Array1: ");
      print(array1);
    int array3[] = inverter2(array2);
      System.out.println("Array3: ");
      print(array3);
  }
  
  public static int[] copy(int[] array)
  {
    int[] result = new int[array.length];
    for(int i = 0; i < array.length; i++)
    {
      result[i] = array[i];
    }
    return result;
  }
  
  public static void inverter(int[] array)
  {
    int temp = 0;
    for(int i = 0; i < array.length/2; i++)
    {
      temp = array[i];
      array[i] = array[array.length - 1 - i];
      array[array.length - 1 - i] = temp;
    }
    System.out.println("Array0: ");
    print(array);
  }
  
  public static int[] inverter2(int[] array)
  {
    int[] result = copy(array);
    int[] invertion = new int[result.length];
    for(int i = result.length-1; i >= 0; i--)
    {
      invertion[i] = result[7-i];
    }
    result = invertion;
    return result;
  }
  
  public static void print(int[] array)
  {
    System.out.print("{");
    for(int i = 0; i < array.length; i++)
    {
      if(i > 0)
      {
        System.out.print(", ");
      }
      System.out.print(array[i]);
    }
    System.out.println("}");
  }
}