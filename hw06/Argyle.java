//CSE 2 Argyle
//Create an argyle pattern
//Larissa Chow
//March 20, 2018

import java.util.Scanner;

public class Argyle
{
    //main method required for every Java program
    public static void main(String[] args)
    {
      Scanner scan = new Scanner(System.in);
      
      System.out.println("Please enter a positive integer for the width of Viewing window in characters: ");
      int viewwidth = scan.nextInt();
        
      System.out.println("Please enter a positive integer for the height of Viewing window in characters: ");
      int viewheight = scan.nextInt();
      
      System.out.println("Please enter a positive integer for the width of the argyle diamonds: ");
      int diamondwidth = scan.nextInt();
      
      System.out.println("Please enter a positive odd integer for the width of the argyle center stripe: ");
      int stripewidth = scan.nextInt();
      
      System.out.println("Please enter a first character for the pattern fill: ");
      char firstchar = scan.next().charAt(0);
      
      System.out.println("Please enter a second character for the pattern fill: ");
      char secondchar = scan.next().charAt(0);
      
      System.out.println("Please enter a third character for the stripe fill: ");
      char stripefill = scan.next().charAt(0);
      
      int duplicatewidth = diamondwidth * 2; //multiplies the diamond width by 2 to get the dimensions of the whole X
      
      //start of the top half of the pattern
      int hashstart = 0;  //left side of the X stripe thickness found on the left side of the pattern(varies depending on thickness)
      int hashend = hashstart + stripewidth - 1;  //right side of the X stripe thickness found on the left side of the pattern
      int plus = duplicatewidth/2;  //variable for the top left side of the diamond behind the X
      int midpoint = duplicatewidth/2;  //splits the pattern in half
      int hashstart2 = duplicatewidth;  //where the X stripe thickness begins on the right side of the pattern
      int hashend2 = duplicatewidth + stripewidth - 1;  //where the X stripe thickness ends on the right side of the pattern
      int plus2 = midpoint - 1; //variable for the top right side of the diamond behind the X
      
      for(int i = 0; i < viewheight/2; i++) //loop for the number of rows, representing the height
      {
        for(int j = 0; j < viewwidth; j++)  //loop for the width of the window
        {
          if(j%duplicatewidth < midpoint)  //top left side
          {
            if(i%duplicatewidth == j%duplicatewidth ||
              hashend-1 >= j%duplicatewidth && hashstart-1 <= j%duplicatewidth)
            {
              System.out.print(stripefill); //prints out the symbol to fill in the X stripes
            }
            else if(plus <= j%duplicatewidth) //as long as j is larger than or equal to plus variable
            {
              System.out.print(secondchar); //prints out the second symbol asked for
            }
            else  //if above requirements are not met
            {
              System.out.print(firstchar);  //prints out the first symbol asked for to fill in spaces
            }
          }
          
          if(j%duplicatewidth >= midpoint )  //top right side
          {
            if(hashstart2-2 <= j%duplicatewidth && hashend2-2 >= j%duplicatewidth)
            {
              System.out.print(stripefill); //prints out the symbol to fill in the X stripes
            }
            else if(plus2 >= j%duplicatewidth)  //as long as j is less than or equal to the plus2 variable
            {
              System.out.print(secondchar); //prints out the second symbol asked for
            }
            else  //if above requirements are not met
            {
              System.out.print(firstchar);  //prints out the first symbol asked for to fill in spaces
            }
          }
        }
        hashstart = hashstart + 1;  //increments hashstart by one to make the symbols forming the X to appear like they are moving diagonally
        hashend = hashend + 1;  //increments hashend by one
        plus = plus - 1;  //decrements plus by 1
        hashstart2 = hashstart2 - 1;  //decrements hashstart2 by 1 for the top right of the pattern
        hashend2 = hashend2 - 1;  //decrements hashend2 by 1 for the top right of the X
        plus2 = plus2 + 1;  //increments plus2 by 1 for the top right diamond
        System.out.println(); //goes to the next line
      } //end of top for loop
      
      
      //start of the bottom half of the pattern
      //notes are the same as above with some variances in numbers and signs
      int hashstart3 = duplicatewidth/2;
      int hashend3 = duplicatewidth/2 + stripewidth - 1;
      int plus3 = 0;
      int hashstart4 = duplicatewidth/2;
      int hashend4 = duplicatewidth/2 + stripewidth - 1;
      int plus4 = duplicatewidth;
      for(int i = 0; i < viewheight/2; i++)
      {
        for(int j = 0; j < viewwidth; j++)
        {
          if(j%duplicatewidth < midpoint)  //top left side
          {
            if(hashend3-2 >= j%duplicatewidth && hashstart3-2 <= j%duplicatewidth)
            {
              System.out.print(stripefill);
            }
            else if(plus3 <= j%duplicatewidth)
            {
              System.out.print(secondchar);
            }
            else
            {
              System.out.print(firstchar);
            }
          }
          
          if(j%duplicatewidth >= midpoint )  //top right side
          {
            if(hashstart4-1 <= j%duplicatewidth && hashend4-1 >= j%duplicatewidth)
            {
              System.out.print(stripefill);
            }
            else if(plus4-1 >= j%duplicatewidth)
            {
              System.out.print(secondchar);
            }
            else
            {
              System.out.print(firstchar);
            }
          }
        }
        hashstart3 = hashstart3 - 1;
        hashend3 = hashend3 - 1;
        plus3 = plus3 + 1;
        hashstart4 = hashstart4 + 1;
        hashend4 = hashend4 + 1;
        plus4 = plus4 - 1;
        System.out.println(); //creates a new line
      } //end of bottom half pattern for loop
    } //end of main method
} //end of class