//CSE 2 _x
//Create a program to hide the secret messagea "X"
//Larissa Chow
//March 9, 2018

import java.util.Scanner;

public class _x 
{
    //main method required for every Java program
    public static void main(String[] args) 
    {
      Scanner scan = new Scanner(System.in);
        
      System.out.println("Type in any integer from 1-100: ");

      while(!scan.hasNextInt()) 
      {
        System.out.println("Please type in any integer from 1-100: ");
        scan.next();
      }
      int input = scan.nextInt();

      while (input > 100 || input < 0) 
      {
        System.out.println("PLEASE type in any integer from 1-100: ");
        input = scan.nextInt();
      }
      
        for(int i = 1; i < input+1; i++)
        {
          for(int j = 1; j <= input; j++)
          {
            if(i == j || i == input+1-j)
            {
              System.out.print(" ");
            } //end of if statement
            else
            {
            System.out.print("*");
            } //end of else statement
          } //end of j for loop
          System.out.println(); //starts a new line
        } //end of i for loop
    } //end of main method
} //end of class