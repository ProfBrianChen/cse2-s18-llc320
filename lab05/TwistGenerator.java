//CSE 2 Twist Generator
//Create a twist using loops
//Larissa Chow
//March 2, 2018

import java.util.Scanner;

public class TwistGenerator
{
    //main method required for every Java program
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Type a positive integer: ");
        
        int length = scan.nextInt();
        int length2 = length;
        int length3 = length;
      
        while (length <= 0)
        {
          System.out.println("Please type in a positive integer: ");
          length = scan.nextInt();
          length2 = length;
          length3 = length;
        }
      
        while (length > 3)
        {
          System.out.print("\\ /");
          length = length - 3;
          continue;
        }
        
        if (length == 1)
        {
          System.out.println("\\");
        } 
        if (length == 2)
        {
          System.out.println("\\ ");
        }
        if (length == 3)
        {
          System.out.println("\\ /");
        }
      
        while (length2 > 3)
        {
          System.out.print(" X ");
          length2 = length2 - 3;
          continue;
        }
        
        if (length2 == 1)
        {
          System.out.println(" ");
        }
        if (length2 == 2)
        {
          System.out.println(" X");
        }
        if (length2 == 3)
        {
          System.out.println(" X ");
        }
      
        while (length3 > 3)
        {
          System.out.print("/ \\");
          length3 = length3 - 3;
          continue;
        }
        
        if (length3 == 1)
        {
          System.out.println("/");
        }
        if (length3 == 2)
        {
          System.out.println("/ ");
        }
        if (length3 == 3)
        {
          System.out.println("/ \\");
        }
    }
}