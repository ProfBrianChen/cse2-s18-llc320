//CSE 2 StringAnalysis
//Processes string by examining all characters, or just a specified # of characters and determines if they are letters
//Larissa Chow
//March 27, 2018

import java.util.Scanner; //scanner import

public class StringAnalysis
{
  public static void main(String[] args)  //main method
  {
    Scanner scan = new Scanner(System.in);  //initializes scanner
    
    System.out.println("Enter any string of letter characters: ");
    String input = scan.next(); //prompts user to type in a string of characters
    while (!input.matches(".*[a-z].*") || input.matches(".*[0-9].*")) 
    //if the string does not have letters a-z or has any numbers in it
    {
      System.out.println("Error! Please print out a string using letters: "); //error statement will continue to show up
      input = scan.next();  //keeps prompting user to input a correct string value
    }
    
    System.out.println("Would you like to specify using an integer how much of the string should be analyzed (yes or no)? ");
    String yesno = scan.next(); //prompts user to answer yes or no to the question
    while (!yesno.equals("yes") && !yesno.equals("no")) //if user types in anything but "yes" or "no"
    {
      System.out.println("Please type out either 'yes' or 'no'. "); //prompts user to type in correct option
      yesno = scan.next();  //keeps prompting user to input a correct option
    }
    
    if(yesno.equals("yes")) //if the user types "yes" (wants to type in own integer value)
    {
      if (Analysis2(input) == true) //if Analysis2 method (accepts int and string) is found to be true
      {
        System.out.println("The analyzed section of the string contains only letter characters.");
        System.exit(0); //exits the script
      }

      if (Analysis(input) == false) //if Analysis2 method (accepts int and string) is found to be false
      {
        System.out.println("The analyzed section of the string does NOT have only letter characters in it!");
      }
    } //end of "yes" option
    if(yesno.equals("no"))  //if the user types "no" (doesn't want to type in own integer value)
    {
      if (Analysis(input) == true)  //if Analysis method (only accepts string) is found to be true
      {
        System.out.println("The string contains only letter characters.");
      }

      if (Analysis(input) == false)   //if Analysis method (only accepts string) is found to be false
      {
        System.out.println("The string does NOT have only letter characters in it!");
      }
    } //end of "no" option
  } //end of main method
  
  
  public static boolean Analysis(String b) //method that only accepts string
  {
    boolean finalresult = true; //sets boolean 'finalresult' initially equal to "true"
    for(int i = 0; i < b.length(); i++) //loops as long as i is less than the input length 
    //("b" will be equal to "input" from main method)
    {
      char result = b.charAt(i);  //sets character 'result' equal to a certain value in the string 'b'
      if (result >= 'a' && result <= 'z') //if 'result' is found to include any letter a to z
      {
        continue; //keep cycling through for loop
      }
      else  //if 'result' is found to include something else other than a through z
      {
        finalresult = false;  //sets 'finalresult' equal to false
        break;  //ends for loop immediately
      }
    } //end of for loop
    return finalresult; //returns the boolean back to main method
  } //end of Analysis Method (only accepts string)
  
  
  public static boolean Analysis2(String c) //method that accepts int and string
  {
    Scanner scan = new Scanner(System.in);  //initializes scanner
    
    System.out.println("Type in an integer: ");
    while (!scan.hasNextInt())  //if the input number is not an integer
    {
      System.out.println("Error! Please print out an integer! "); //will print out an error message
      scan.next();  //keeps prompting the user for an integer until given
    }
    int number = scan.nextInt();  //sets the input integer value to a variable
    
    boolean finalresult = true; //sets boolean 'finalresult' initially equal to "true"
    if(number > c.length()) //if the input integer value is greater than the length of the input string from main method
    //("c" will be equal to "input" from main method)
    {
      for(int i = 0; i < c.length(); i++) //loops as long as i is less than the input length
      {
        char result = c.charAt(i); //sets character 'result' equal to a certain value in the string 'c'
        if (result >= 'a' && result <= 'z') //if 'result' is found to include any letter a to z
        {
          continue; //keep cycling through for loop
        }
        else  //if 'result' is found to include something else other than a through z
        {
          finalresult = false;  //sets 'finalresult' equal to false
          break;  //ends for loop immediately
        }
      } //end of for loop
      return finalresult; //returns the boolean back to main method
    } //end of scenario where input integer number is greater than input length
    
    if(number <= c.length())  //if integer is less than or equal to the input length from the main method
    {
      for(int i = 0; i < number; i++) //loops as long as i is less than the integer value
      {
        char result = c.charAt(i);  //sets character 'result' equal to a certain value in the string 'c'
        if (result >= 'a' && result <= 'z') //if 'result' is found to include any letter a to z
        {
          continue; //keep cycling through for loop
        }
        else  //if 'result' is found to include something else other than a through z
        {
          finalresult = false;  //sets 'finalresult' equal to false
          break;  //ends for loop immediately
        }
      } //end of for loop
    } //end of scenario where integer is less than or equal to the input length
    return finalresult; //returns the boolean back to main method
  } //end of Analysis2 Method
} //end of class