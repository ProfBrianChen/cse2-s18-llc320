//CSE 2 Area
//Ask the user for a shape and calculate the area of that shape
//Larissa Chow
//March 27, 2018

import java.util.Scanner; //scanner import

public class Area
{
  public static void main(String[] args)  //main method
  {
    Scanner scan = new Scanner(System.in);  //initializes scanner
    
    System.out.println("Please type in a shape: ");
    String shapeinput = scan.next();  //prompts user to type in a shape
    while (!shapeinput.equals("rectangle") && 
           !shapeinput.equals("triangle") && 
           !shapeinput.equals("circle"))  //if shape input is not equal to rectangle, triangle, or circle
    {
      System.out.println("Please choose either a rectangle, triangle, or circle: ");
      //statement pops up telling user correct inputs
      shapeinput = scan.next(); //prompts user to input a correct shape
    }
    
    double area = Input(shapeinput);  //calls upon input method and sets it equal to 'area'
    if(shapeinput.equals("rectangle"))  //if the shape is a rectangle
    {
      System.out.println("The area of the rectangle is " + area + "."); //prints the area of the rectangle
    }
    if(shapeinput.equals("triangle")) //if the shape is a triangle
    {
      System.out.println("The area of the triangle is " + area + ".");  //prints the area of the triangle
    }
    if(shapeinput.equals("circle")) //if the shape is a circle
    {
      System.out.println("The area of the circle is " + area + ".");  //prints the area of the circle
    }
  } //end of main method
  
  public static double Input(String muchwowmuchshape) //input method
  {
    Scanner scan = new Scanner(System.in);  //initializes the scanner
    
    double area = 0;
    if(muchwowmuchshape.equals("rectangle"))  //if the shape is equal to a rectangle
    {
      System.out.println("Please type in the length of the rectangle: ");
      while (!scan.hasNextDouble()) //if the rectangle length is not a number
      {
        System.out.println("Error! Please provide a numerical value: ");  //error statement pop up
        scan.next();  //keeps prompting the user to input a numerical value
      }
      double a = scan.nextDouble(); //stores the input value of the rectangle length

      System.out.println("Please type in the width of the rectangle: ");
      while (!scan.hasNextDouble()) //if the rectangle width is not a number
      {
        System.out.println("Error! Please provide a numerical value: ");  //error statement pop up
        scan.next();  //keeps prompting the user to input a numerical value
      }
      double b = scan.nextDouble(); //stores the input value of the rectangle width
      area = Rectangle(a, b); //calls the rectangle method to solve for the area of the rectangle
    } //end of rectangle if statement
    
    
    if(muchwowmuchshape.equals("triangle"))//if the shape is equal to a triangle
    {
      System.out.println("Please type in the base value of the triangle: ");
      while (!scan.hasNextDouble()) //if the triangle base is not a number
      {
        System.out.println("Error! Please provide a numerical value: ");  //error statement pop up
        scan.next();  //keeps prompting the user to input a numerical value
      }
      double a = scan.nextDouble(); //stores the input value of the triangle base

      System.out.println("Please type in the height of the triangle: ");
      while (!scan.hasNextDouble()) //if the triangle height is not a number
      {
        System.out.println("Error! Please provide a numerical value: ");  //error statement pop up
        scan.next();  //keeps prompting the user to input a numerical value
      }
      double b = scan.nextDouble(); //stores the input value of the triangle height
      area = Triangle(a, b);  //calls the triangle method to solve for the area of the triangle
    } //end of triangle if statement
    
    
    if(muchwowmuchshape.equals("circle")) //if the shape is equal to a circle
    {
      System.out.println("Please input the circle radius: ");
      while (!scan.hasNextDouble()) //if the circle radius input is not a number
      {
        System.out.println("Error! Please provide a numerical value: ");  //error statement pop up
        scan.next();  //keeps prompting the user to input a numerical value
      }
      double a = scan.nextDouble(); //stores the input value of the circle radius
      area = Circle(a); //calls the circle method to solve for the area of the circle
    } //end of circle if statement
    return area;  //returns the area of the shape back to the main method
  } //end of Input method
  
  
  public static double Rectangle(double length, double width) //rectangle method
  {
    return length * width;  //formula for the area of a rectangle
  } //end of Rectangle method
  
  public static double Triangle(double base, double height) //triangle method
  {
    return base * height * 0.5; //formula for the area of a triangle
  } //end of Triangle method
  
  public static double Circle(double radius)  //circle method
  {
    return Math.PI * Math.pow(radius, 2); //formula for the area of a circle
  } //end of Circle method
} //end of class