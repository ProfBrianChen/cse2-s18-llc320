Grade:     85/100

CSE2Linear Comments: 
A) Does the code compile?  How can any compiler errors be resolved?
Yes.
B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
No. Linear search does not always find element.
C) How can any runtime errors be resolved?
Improve linear search algorithm.
D) What topics should the student study in order to avoid the errors they made in this homework?
N/A
E) Other comments:
Good use of comments and good formatting.

CSE2Linear Comments: 
A) Does the code compile?  How can any compiler errors be resolved?
Yes.
B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
No. Does not remove target value.
C) How can any runtime errors be resolved?
When removing the target value, it should remove the actual element with that value, not the index.
D) What topics should the student study in order to avoid the errors they made in this homework?
N/A
E) Other comments:
Good formatting.
