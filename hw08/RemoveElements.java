//CSE 2 RemoveElements
//
//Larissa Chow
//April 10, 2018

import java.util.Scanner;
import java.util.Random;

public class RemoveElements
{
  public static void main(String [] arg)
  {
	  Scanner scan = new Scanner(System.in);
    int num[] = new int[10];	//ensures that grades array only has 15 ints
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer = "";
	  do
    {
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput(num);	//sets num equal to randomInput method
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);

      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);	//sets newArray1 equal to delete method
      String out1= "The output array is ";
      out1 += listArray2(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
 
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);	//sets newArray2 equal to remove method
      String out2 = "The output array is ";
      out2 += listArray3(newArray2, target); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);

      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");	//prompts user if they wantto continue
      answer = scan.next();
	  }
    while(answer.equals("Y") || answer.equals("y"));	//replays code
  }	//end of main method
 
  public static String listArray(int num[])	//prints out random numbers in specific format
  {
	  String out = "{";
	  for(int j = 0; j < num.length; j++)
    {
      if(j > 0)
      {
        out += ", ";
      }
      out += num[j];
	  }
	  out += "} ";
	  return out;
  }	//end of listArray method
  
  public static int[] randomInput(int num[])	//generates random numbers
  {
    Random rnd = new Random();
		int[] randomInt = num;
		for(int i = 0; i < num.length; i++)
		{
			randomInt[i] = rnd.nextInt(10);	//ensures that random number generated is less than 10
		}
    return randomInt;
  }	//end of randomInput method
  
  public static int[] delete(int list[], int pos)	//deletes one of the random numbers generated in certain position
	{
		int bellpepper = 0;
		int tomato = 0;
    for(int i = 0; i < list.length; i++)
    {
      if(i == pos)	//if one of the random numbers is equal to user input
      {
       	bellpepper = -2;
				tomato = i;
				break;
      }
			else
			{
				bellpepper = -1;
			}
		}
		if(bellpepper == -2)
		{
			for(int i = tomato+1; i < list.length; i++)
			{
				list[i-1] = list[i];	//does not print number equal to user input
			}
		}
		return list;
  }		//end of delete method
	
	public static String listArray2(int num[])	//prints out random numbers in specific format
  {
	  String out = "{";
	  for(int j = 0; j < num.length - 1; j++)
    {
      if(j > 0)
      {
        out += ", ";
      }
      out += num[j];
	  }
	  out += "} ";
	  return out;
  }	//end of listArray2 method
	
	public static int[] remove(int list[], int target)	//removes all of a number specified by user
	{
		for(int i = target; i < list.length -1; i++)	//while i is equal to the user input number
		{
			int sun = 0;
			list[i] = sun + list[i+1];	//does not print out the user input number
			sun = list[i + 1];
		}
		return list;
	}	//end of remove method
	
	public static String listArray3(int num[], int target)	//prints out random numbers in specific format
  {
		int potato = 0;
		for(int i = 0; i < num.length; i++)
		{
			if(num[i] == target)
			{
				potato += 1;
			}
		}
	  String out = "{";
	  for(int j = 0; j < num.length - potato - 1; j++)
    {
      if(j > 0)
      {
        out += ", ";
      }
      out += num[j];
	  }
	  out += "} ";
	  return out;
  }	//end of listArray3 method
}	//end of class