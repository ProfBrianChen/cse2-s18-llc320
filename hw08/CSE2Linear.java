//CSE 2 CSE2Linear
//
//Larissa Chow
//April 10, 2018

import java.util.Scanner;
import java.util.Random;

public class CSE2Linear
{
  public static void main(String[] args)
  {
    Scanner scan = new Scanner(System.in);  //implements scanner
    int[] grades = new int[15]; //ensures that grades array only has 15 ints
    
    int boop;
    System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
    for(int i = 0; i < grades.length; i++)
    {
      while(!scan.hasNextInt()) //if user input not an int
      {
        System.out.println("Error! You did not enter an integer."); //prints out error message
        scan.next();  //keeps prompting user until an int is input
      }
      grades[i] = scan.nextInt(); //sets grades array equal to the input numbers
      while(grades[i] < 0 || grades[i] > 100) //if user input is not between 0 and 100
      {
        System.out.println("Error! You did not enter an integer between 0 - 100."); //prints out error message
        grades[i] = scan.nextInt(); //keeps prompting user until a correct number is input
      }
    } //end of for loop
    for(int i = 0; i < grades.length; i++)
    {
      for(int j = i + 1; j < grades.length; j++)
      {
        if(grades[i] > grades[j]) //organizes user unput grades into ascending order
        {
          boop = grades[i];
          grades[i] = grades[j];
          grades[j] = boop;
        }
      }
    } //end of for loop
    System.out.println("These are the final grades that you have input in ascending order: ");
    for(int i = 0; i < grades.length; i++)
    {
      System.out.print(grades[i] + " ");  //prints out the ascending grades
    }
    System.out.println(); //new line
    System.out.println("Enter a grade to searched for: ");
    int search = scan.nextInt();  //user input
    
    int elevator = grades.length;
    int choochoo = binarySearch(grades, search, elevator);  //calls method binarySearch
    if(choochoo == 123456)  //if user input equals a number from one of the input grades
    {
      System.out.println(search + " was found in the list");
    }
    if(choochoo == -444)  //if user input does not equal a number from one of the input grades
    {
      System.out.println(search + " was NOT found in the list");
    }
    
    scramble(grades); //calls method scramble
    
    System.out.println(); //new line
    System.out.println("Enter a grade to searched for: ");
    int search2 = scan.nextInt(); //user input
    
    linearSearch(grades, search2);  //calls method linearSearch
  }
  
  public static int binarySearch(int[] grades, int search, int escalator)
  {
    int middle = 1 + (escalator - 1)/2; //find the middle number out of all the input grades
    
    for(int i = 0; i < escalator; i++)
    {
      if(grades[middle] == search)  //if search grade is in the middle
      {
        return 123456;
      }
      
      if(grades[middle] > search) //if search grade is lower value than middle
      {
        return binarySearch(grades, search, escalator-1);
      }
      return binarySearch(grades, search, escalator+1); //if search grade is higher value than middle
    }
    return -444;
  } //end of binarySearch method
  
  public static void scramble(int[] grades)
  {
    int first, second;
    Random rnd = new Random();
    for(int i = grades.length - 1; i>0; i--)  //scrambles user input
    {
      first = rnd.nextInt(i+1);
      second = grades[first];
      grades[first] = grades[i];
      grades[i] = second;
    }
    System.out.println("Scrambled:");
    for(int i = 0; i < grades.length; i++)
    {
      System.out.print(grades[i] + " ");  //prints out scrambled user input
    }
  } //end of scramble method
  
  public static void linearSearch(int[] grades, int search2)
  {
    int sleep = 0;
    for(int i = 0; i < grades.length; i++)
    {
      if(grades[i] == search2)  //if one of the grades is equal the search value
      {
        System.out.println(search2 + " was found in the scrambled list with " + (i+1) + " iterations");
      }
      else  //if one of the grades is not equal to the search value
      {
        sleep += 1; //adds one to sleep variable
      }
    }
    if(sleep == grades.length)  //if no grades were found to be equivalent to search value
    {
      System.out.println(search2 + " was not found in the scrambled list");
    }
  } //end of linearSearch method
} //end of class